
const mongoose = require('mongoose');
const User = require("../models/User");
const passport = require('passport');
const _ = require('lodash');
const ObjectId = require("mongodb").ObjectId;

module.exports.authenticate = (req, res, next) => {
    // call for passport authentication
    passport.authenticate('local', (err, user, info) => {
        // error from passport middleware
        if (err) return res.status(404).json(err);
        // registered user
        if (user) return res.status(200).json({ "token": user.generateJwt() });
        // unknown user or wrong password
        else return res.status(401).json(info);
    })(req, res);
}

module.exports.userProfile = (req, res, next) =>{
    User.findOne({ _id: req._id },
        (err, user) => {
            if (!user)
                return res.status(404).json({ status: false, message: 'User record not found.' });
            else
                return res.status(200).json({ status: true, user : _.pick(user,['name','image','email','_id','friendsList','request']) });
        }
    );
}

    module.exports.register =  async (req,res,next)=>{
        const {name,age,email,password} = req.body;
        let user= new User();
        user.name = name;         
        user.age = age;
        user.email = email;
        user.password = password;
    
        user.save((err,doc)=>{
            if(!err) res.send(doc)
            else {
                //console.log("body",doc,err)
                if(err.code === 11000) res.status(422).send("Duplicate email address")
                else return next(err)
            }
        });
        
    }
    module.exports.getUserById = (req, res, next) =>{
        console.log("param",req.query)
        User.findOne({ _id: req.query._id },
            (err, user) => {
                if (!user)
                    return res.status(404).json({ status: false, message: 'User record not found.' });
                else
                    return res.status(200).json({ status: true, user : _.pick(user,['name','image','email','_id','friendsList','request']) });
            }
        );
    }
    
    module.exports.getUserByName = (req, res, next) =>{
        console.log("param",req.query)
        User.findOne({ name: req.query.name },
            (err, user) => {
                if (!user)
                    return res.status(200).json({ status: false });
                else
                    return res.status(200).json({ status: true, user : _.pick(user,['name','image','email','_id','friendsList','request']) });
            }
        );
    }
    module.exports.sendRequest =  async (req,res,next)=>{
        const {_id,user_id,name} = req.body; 
        console.log("body",req.body)
        let request =[]
        let user = await User.findOne({ _id:user_id}).lean();  
        console.log("user",user) 
        request = user.request;
        request.push({
            userId:_id,
            username:name
        })    
        let totalRequest = user.totalRequest;
        totalRequest++;
        const updateUser = await User.update({ _id:user_id }, { "$set": { request,totalRequest } });
        // console.log("update",updateUser)
        return res.status(200).send(updateUser);
        
    }
    module.exports.rejectRequest =  async (req,res,next)=>{
        const {_id,name,user_id,user_name} = req.body; 
        console.log("body",req.body)
        let request = [];
        let user = await User.findOne({ _id:_id}).lean();        
            
            request =user.request.filter(arr => {
                console.log(arr.userId,req.body.user_id,arr.userId.toString() === req.body.user_id)
                return arr.userId.toString() !== req.body.user_id;
            });
            console.log("request",request)
        let totalRequest = user.totalRequest;
        totalRequest--;
        const updateUser = await User.update({ _id:_id }, { "$set": { request,totalRequest } });
        console.log("update",updateUser)
        return res.status(200).send(updateUser);
        
    }
    module.exports.approveRequest =  async (req,res,next)=>{
        const {_id,name,user_id,user_name} = req.body; 
        console.log("body",req.body)
        let request = [];
        let friendsList1 = [];
        let friendsList2 = []
        let user = await User.findOne({ _id:_id}).lean();
        request =user.request.filter(arr => {
            console.log(arr.userId,req.body.user_id,arr.userId.toString() === req.body.user_id)
            return arr.userId.toString() !== req.body.user_id;
        });
        console.log("request",request)
        
         friendsList1 = user.friendsList
        friendsList1.push({
            friendId:user_id,
            friendName:user_name
        })
        let totalRequest = user.totalRequest;
        totalRequest--;
        const updateUser1 = await User.update({ _id:_id }, { "$set": { request,friendsList:friendsList1,totalRequest} });

        let frienduser = await User.findOne({ _id:new ObjectId(user_id)}).lean();
        friendsList2 = frienduser.friendsList
        friendsList2.push({
            friendId:_id,
            friendName:name
        })
        const updateUser2 = await User.update({ _id:user_id }, { "$set": {friendsList:friendsList2} });

        return res.status(200).send(updateUser1);
        
    }
    module.exports.changeProfile =  async (req,res,next)=>{
        const {imageUrl,_id} = req.body; 
        console.log("body",req.body)
        let request = [];
        let user = await User.findOne({ _id:_id}).lean();        
          
        const updateUser = await User.update({ _id:_id }, { "$set": { image:imageUrl } });
        console.log("update",updateUser)
        return res.status(200).send(updateUser);
        
    }

