const mongoose = require('mongoose');
const User = require("../models/User");
const Post = require("../models/Post");
const ObjectId = require("mongodb").ObjectId;
const ObjectID = require("bson");


module.exports.savePost =  async (req,res,next)=>{
    const {data,image,user_id} = req.body;
    console.log("body", new ObjectId(user_id))
    let post= new Post();
    post.data = data;         
    post.userId = new ObjectId(user_id);
    post.image =image;

    post.save((err,doc)=>{
        if(!err) res.send(doc)
        else return next(err)
    });
    
}
module.exports.allPosts =  async (req,res,next)=>{
    try {
        const populate = {
                    path: "userId",
                    select: "name",
                    model: User
                  };
        return res.status(200).send(await Post.find().sort("-createdAt").populate(populate));
    } catch (e) {
        console.log(e);
        return next(e);
    }
    
}
module.exports.getPostsByUser = (req, res, next) =>{
    Post.find({userId: req.query._id },
        (err, post) => {
            if (!post)
                return res.status(404).json({ status: false, message: 'No  records found.' });
            else
                return res.status(200).json({ status: true, post : post});
        }
    );
}
