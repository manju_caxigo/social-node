const express = require('express');
const router = express.Router();

const ctrlUser = require("../controllers/user.controller")
const ctrlPost = require("../controllers/post.controller")
const jwtHelper = require('../config/jwtHelper');
const multer = require("multer")

router.post('/register',ctrlUser.register);
router.post('/authenticate', ctrlUser.authenticate);
router.get('/userProfile',jwtHelper.verifyJwtToken, ctrlUser.userProfile);
router.post('/changeProfile',ctrlUser.changeProfile);
router.get('/getUserById',ctrlUser.getUserById);
router.get('/getUserByName',ctrlUser.getUserByName);
router.post('/sendRequest',ctrlUser.sendRequest);
router.post('/approveRequest',ctrlUser.approveRequest);
router.post('/rejectRequest',ctrlUser.rejectRequest);



router.post('/savePost',ctrlPost.savePost);
router.get('/allPosts',ctrlPost.allPosts);
router.get('/getPostsByUser',ctrlPost.getPostsByUser);



module.exports = router;
//
