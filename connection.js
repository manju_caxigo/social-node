const mongoose = require ("mongoose");

//MongoDB connection
const URI = "mongodb+srv://dbUser:dbUser@cluster0.t29cm.mongodb.net/dbUser?retryWrites=true&w=majority";

const connectDB = async () => {
 await mongoose.connect(URI,{ 
    useUnifiedTopology: true, 
    useNewUrlParser: true });
 console.log("db connected")
}
mongoose.set("useCreateIndex", true);

module.exports = connectDB;