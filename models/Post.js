const mongoose = require('mongoose');

var Post = new mongoose.Schema({
    data:String,
    image:String,
    userId: {
        type: mongoose.Schema.Types.ObjectId, ref: 'User'
    },
    
    
},{
    timestamps: { createdAt: true, updatedAt: false }
  })


module.exports = Post = mongoose.model('Post',Post);