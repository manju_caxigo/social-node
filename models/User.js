const mongoose = require('mongoose');
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');

var User = new mongoose.Schema({
    name:{
        type:String,
        required:"Name can't be empty"
    },
    image:String,
    age:{
        type:Number,
        required:"Age can't be empty"

    },
    email:{
        type:String,
        required:"Email can't be empty",
        unique:true
    },
    password:{
        type:String,
        required:"Password can't be empty",
        minlength:[4,"Password must be atleast 4 characters long"]

    },
    saltSecret:String,
    sentRequest:[{
        userId: {
            type: mongoose.Schema.Types.ObjectId, ref: 'User'
        },
        username:{
             type: String, default: ''
        }
    }],
    request: [{
        userId: {
            type: mongoose.Schema.Types.ObjectId, ref: 'User'
        },
        username: {
            type: String, default: ''
        }
    }],
    friendsList: [{
        friendId: {
            type: mongoose.Schema.Types.ObjectId, ref: 'User'
        },
        friendName: {
            type: String, default: ''
        }
    }],
    totalRequest: {
        type: Number, default:0
    }
},{
    timestamps: { createdAt: true, updatedAt: false }
  })

//Email Validation

User.path('email').validate((val)=>{
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
},'Invalid Email')


// Password encrypt middleware.
 
User.pre("save", function save(next) {
    const user = this;
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        return next(err);
      }
      bcrypt.hash(
        user.password,
        salt,
        (err, hash) => {
          if (err) {
            return next(err);
          }
          user.password = hash;
          user.saltSecret = salt;
          next();
        }
      );
    });
  });
  
  User.methods.verifyPassword = function (password){
      return bcrypt.compareSync(password,this.password)
  }
  User.methods.generateJwt = function () {
    return jwt.sign({
        _id: this._id
    }, process.env.JWT_SECRET,
        {
            expiresIn: process.env.JWT_EXP
        });
}

module.exports = User = mongoose.model('User',User);