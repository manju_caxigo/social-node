require('./config/config');
require('./config/passportConfig');

const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
var fileExtension = require('file-extension')
const cors = require('cors');
const connectDB = require("./connection");
const routes = require('./routes/index.router')
const passport = require('passport');



const app = express();


connectDB();
app.use(bodyParser.json());
app.use(cors());
app.use(passport.initialize());
app.use('/uploads', express.static('uploads'));
app.use("/api",routes);

const port=process.env.PORT || 3000;
// Configure Storage
var storage = multer.diskStorage({

    // Setting directory on disk to save uploaded files
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },

    // Setting name of file saved
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + fileExtension(file.originalname))
    }
})
var upload = multer({
    storage: storage,
    limits: {
        // Setting Image Size Limit to 2MBs
        fileSize: 2000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            //Error 
            cb(new Error('Please upload JPG and PNG images only!'))
        }
        //Success 
        cb(undefined, true)
    }
})

app.post('/api/uploadfile', upload.single('uploadedImage'), (req, res, next) => {
    const file = req.file
    console.log(req);
    if (!file) {
        const error = new Error('Please upload a file')
        error.httpStatusCode = 400
        return next(error)
    }
    res.status(200).send({
        statusCode: 200,
        status: 'success',
        uploadedFile: file
    })

}, (error, req, res, next) => {
    res.status(400).send({
        error: error.message
    })
})

//error handler
app.use((err,req,res,next)=>{
    if(err.name === 'ValidationError'){
        var valErrors =[];
        Object.keys(err.errors).forEach(key=>valErrors.push(err.errors[key].message));
        res.status(422).send(valErrors);
    }
})

app.listen(port,()=>{
    console.log(`Server Started at port: ${port}`)
})
